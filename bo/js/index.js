//global Variables will persist in the same HTML Though different scripts
currentLocation="";
let configData //will be avaible in the page if not change location
$.getJSON("/config/config.json",(data)=>{
    //once we have the API URL we do important stuff
    configData=data.development;
});
$(document).ready(()=>{
let pathname = window.location.pathname; // Returns path only (/path/example.html)
let url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
let origin   = window.location.origin;   // Returns base URL (https://example.com)

loadPartials();
/*$.handlebars({
    templatePath: '/bo/partials/',
    templateExtension: 'part'
});
$('main').render('mainContent', {
    // ...
});*/
    $('.layer').on('click',(element)=>{
        $(element.currentTarget).removeClass('active');
        $('ul[class*="active"]').removeClass('active');
    })
});

function loadPartials(){
    $('nav').load('/bo/partials/navBar.part', ()=>{});  
    $('main').load('/bo/partials/contents/mainContent.part', ()=>{
        $.getScript("/bo/js/script.js",(data)=>{})
    });
    $('footer').load('/bo/partials/footer.part', ()=>{});
    $('aside').load('/bo/partials/sideBar.part', ()=>{ });
}

function navigate(element,to){
   
    switch (to) {
        case "./":
            currentLocation="principal"
            $('main').load('/bo/partials/contents/mainContent.part', ()=>{
                $.getScript("/bo/js/script.js",(data)=>{ })
            });
            break;
        case "rafflesControl.html#create":
        case "rafflesControl.html#update":
            if(currentLocation.split("#")[0]!=="rafflesControl.html"){
                $('main').load('/bo/partials/contents/rafflesControlContent.part', ()=>{
                    $.getScript("/bo/js/raffles.js",(data)=>{ })
                    currentLocation=to;
                    //console.log(currentLocation.split("#")[1])
                });
            }else{//significa que ya esta en rifas y solo se esta despalzando a otra zona de la pagina
                //TODO: Scrollear a la parte de la pagina que pide el usuario
            }        
            break;
        case "usuarios":
            if(currentLocation.split("#")[0]!=="usuarios"){
                $('main').load("/bo/partials/contents/usersContent.part",()=>{
                    currentLocation=to;
                })
            }else{//significa que ya esta en rifas y solo se esta despalzando a otra zona de la pagina
                //TODO: Scrollear a la parte de la pagina que pide el usuario
            }            
            break;
        default:
            break;
    }
    
    $(".active").removeClass('active');
    $(element).addClass('active');
    let parent=$(element).parents('ul').siblings('a');
    if(parent){
        parent.addClass('active');
    }
 
}
