
const navbar=$('.navbar');
$(document).ready(()=>{
    changeNavBarColor();
    //load Banner Dinamically
    var imageUrl="../public/images/landing.jpg";
    //$('.content-banner').css("background-image", "url("+imageUrl+")");
    $(".navbar-toggler").on('click', changeNavBarColor);
});

$(document).on('scroll',()=>{
    changeNavBarColor();
});

/* Code for changing active 
            link on clicking*/
            var btns = 
                $("#navigation .navbar-nav .nav-link");
  
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click",
                                      function () {
                    var current = document
                        .getElementsByClassName("active");
  
                    current[0].className = current[0]
                        .className.replace(" active", "");
  
                    this.className += " active";
                });
            } 
  
            /* Code for changing active 
            link on Scrolling */
            $(window).scroll(function () {
                var distance = $(window).scrollTop();
                $('section').each(function (i) {
  
                    if ($(this).position().top 
                        <= distance + 500) {
                          
                            $('.navbar-nav a.active')
                                .removeClass('active');
  
                            $('.navbar-nav a').eq(i)
                                .addClass('active');
                    }
                });
            }).scroll();

function changeNavBarColor(){
    //console.log(screen.orientation)
    if($(".navbar-toggler").css('display')!=='none'){//estamos con menu hamburgesa
        if(window.scrollY<=50 && $(".navbar-toggler").attr('aria-expanded')==='true'){//muy arriba menu abierto            
            dark();
        }else if(window.scrollY<=50 && $(".navbar-toggler").attr('aria-expanded')==='false'){//muy arriba menu cerrado
            transparent();
        }else{
            dark();
        }    
    }else{//sin menu hamburguesaS
        if(window.scrollY<=50){// signinfica que estas muy arriba
            transparent();
        }else {//en este caso yabajaste
            dark();
        }
    }    
}


function transparent(){
    navbar.addClass(' bg-transparent navbar-dark');
    navbar.removeClass(' bg-light navbar-light');
}
function dark(){
    navbar.removeClass(' bg-transparent navbar-dark');
    navbar.addClass('bg-light navbar-light');
}