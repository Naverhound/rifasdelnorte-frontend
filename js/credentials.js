$(document).ready(()=>{
    $('section').justFlipIt({
        Click:'.fliper',
        Template:'<div class="sign-in">'
        +'<h1 class=" text-light text-center fs-2 fw-bolder">Inicia sesión en tu cuenta</h1>'
        +'<p class=" text-light text-center fst-italic fs-5">¡Entra a tu cuenta de <strong ><a href="/" class="text-decoration-none text-light">Sorteos del Norte</a></strong> y ofrece tus rifas!</p>'
        +'<form class="sign-up-form form" action="" method="">'
        +'  <label class="form-label-wrapper mb-2">'
        +'    <p class="form-label fw-bolder">Correo Electrónico</p>'
        +'    <input class="form-control border-bottom border border-2 rounded bg-secondary  bg-opacity-25 fs-5" type="email" placeholder="Ejemplo: correoel@servidores.com" required>'
        +'  </label>'
        +'  <label class="form-label-wrapper">'
        +'    <p class="form-label fw-bolder">Contraseña</p>'
        +'    <input class="form-control border-bottom border border-2 rounded bg-secondary  bg-opacity-25 fs-5" type="password" placeholder="L@ContraseñaActual" required>'
        +'  </label>'
        +'  <a class="" href="##">¿Contraseña olvidada?</a>'
        +'  <label class="form-checkbox-wrapper pt-3">'
        +'    <input class="form-checkbox" type="checkbox" required>'
        +'    <span class="form-checkbox-label">Recordarme la próxima vez</span>'
        +'  </label>'
        +'  <a href="bo/" class="form-btn primary-default-btn transparent-btn text-decoration-none" type="button" >Iniciar Sesión</a>'
        +'  <p class="text-center m-0 pt-3">'
        +'    <a id="fliper" class="text-decoration-none fliper">¿Aún no tienes cuenta? Creemos una: --></a>'
        +'  </p>'
        +'</form>'
      +'</div>'
    });
    
})


function flip(){

}
function unflip(){

}