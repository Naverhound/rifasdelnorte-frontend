
let chosenNumbers=[];
let givenNumbers=[];
let ocupiedNumbers=[];
const tcost=128.50;
const chances=2;
let ticketLimit=0;
const ticketAmount=1000-1;
let ticketsToRender=0;
let debt=0;
const dataResumeModal = new bootstrap.Modal(document.getElementById('dataResumeModal'), {
    keyboard: false
  });
var sucessPurchaseModal = new bootstrap.Modal(document.getElementById('sucessPurchaseModal'), {
    keyboard: false
  });
 
$(document).ready(()=>{
    $('#buyingControls').on('scroll',()=>{
       
    });
    //Datos generales de la rifa
    $('#ginfo').append('<h3 id="title">-Rifa del netthooth-</h3>'
    +'<p id="description">Describimos que el netthooth esta en rifa por pusilanime. Por eso es que Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis doloribus, quidem sed eum molestias culpa ullam, iusto ut sequi perspiciatis possimus porro excepturi, accusantium quae assumenda laborum alias accusamus rem.</p>'
    +'<h5 id="tcost" class="fst-italic text-decoration-underline">Precio Por boleto: $</h5>');
      //agregamos las oportunidades por boleto consultadas
    switch (chances) {
        case 1:
            $('#chances').append('<p class="ms-3">1 Oportunidades (1 número seleccionado no otorga nada extra)</p>');  
            ticketLimit=10;
            ticketsToRender=ticketAmount;
            break;
        case 2:
            $('#chances').append('<p class="ms-3">2 Oportunidades (1 número seleccionado da 1 al azar)</p>');
            ticketLimit=5;
            ticketsToRender=Math.floor(ticketAmount/2);
            break;
        case 3:
            $('#chances').append('<p class="ms-3">3 Oportunidades (1 número seleccionado da 2 al azar)</p>');
            ticketLimit=3;
            ticketsToRender=Math.floor(ticketAmount/3);
            break;
        case 4:
            $('#chances').append('<p class="ms-3">4 Oportunidades (1 número seleccionado da 3 al azar)</p>');
            ticketLimit=2;
            ticketsToRender=Math.floor(ticketAmount/4);
            break;
        default:
            break;
    }
    //agregamos los descuentos consultados de la rifa
    $('#discounts').append('<p class="ms-3">2x$100</p>'+
    '<p class="ms-3">3x$135</p>');
    //agregar oportunidades de la rifa por boleto
    $('#tcost').append("<span >"+tcost+" M.N.</span>");
    //cargamos los numeros para elegir
    loadTickets();
    validationConfig();
    $('#folioForm').find('input[name="cellphone"]').on('keyup',
        function(keyObject){
            var value=$(this).val();
            //this es igual al input Cellphone, es el input con este evento de KeyUp
            if(keyObject.key!='Backspace'){
                switch (value.length) {
                    case 3:
                        $(this).val(value+'-');
                        break;
                    case 7:
                        $(this).val(value+'-');
                        break;
                    default:
                        break;
                }
                //console.log(value.length);
            }
            
    });
    console.log(ticketsToRender)
});
/* Code for changing active 
            link on clicking */
            var btns = 
                $("#navigation .navbar-nav .nav-link");
  
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click",
                                      function () {
                    var current = document
                        .getElementsByClassName("active");
  
                    current[0].className = current[0]
                        .className.replace(" active", "");
  
                    this.className += " active";
                });
            }
  
            /* Code for changing active 
            link on Scrolling */
            $(window).scroll(function () {
                var distance = $(window).scrollTop();
                $('section').each(function (i) {
  
                    if ($(this).position().top 
                        <= distance + 250) {
                          
                            $('.navbar-nav a.active')
                                .removeClass('active');
  
                            $('.navbar-nav a').eq(i+1)
                                .addClass('active');
                    }
                });
            }).scroll();
function testing() {
    alert('No me aplastes')
}
function showResumeModal(){
    debt=chosenNumbers.length*tcost;
    if($('#folioForm').valid()){
        $('#dataResumeModal #names').html($('#folioForm').find('input[name="fnames"]').val());
        $('#dataResumeModal #chosenNumbers').html($('#currentTickets').html());
        $('#dataResumeModal #givenNumbers').html($('#givenTicketsZone').html());
        $('#chosenNumbers .cancelNumber').remove();
        $('#dataResumeModal #usrFullName').html($('#folioForm').find('input[name="fnames"]').val()+' '+$('#folioForm').find('input[name="lnames"]').val());
        $('#dataResumeModal #usrCellphone').html($('#folioForm').find('input[name="cellphone"]').val());
        $('#dataResumeModal #usrState').html($('#folioForm').find('select[name="state"]').val());
        $('#dataResumeModal  #debt').html('$'+debt+' M.N');
        dataResumeModal.show();
    }
}

function reservate() {
    var serializedform=$('#folioForm').serialize();
    var success=true;    
    if(success){
        dataResumeModal.hide();
        sucessPurchaseModal.show();
        $('#sucessPurchaseModal #chosen').html($('#dataResumeModal #chosenNumbers').html());
        $('#sucessPurchaseModal #given').html($('#dataResumeModal #givenNumbers').html());
        $('#sucessPurchaseModal #given div').remove();
        $('#sucessPurchaseModal #folio').html('#12359');
        $('#sucessPurchaseModal #debt').append('$'+debt+' M.N');
    } else{

    }
}
function removeNumber(who){   
    var wholeTicketObject=$(who).parent();
    var ticketNumber=$(wholeTicketObject).attr('id');
    chosenNumbers.splice(chosenNumbers.indexOf(ticketNumber),1);
    $('#ticketsContainer').find('label[id="'+ticketNumber+'"]').removeClass('bg-secondary text-light disabled');
    
    if(givenNumbers.length){        
        for (let i = 0; i < chances-1; i++) {
            var labelsGivenNumbers=$('#givenTicketsZone').find('label');
            var itemIndex = Math.floor(Math.random()*labelsGivenNumbers.length);            
            givenNumbers.splice(itemIndex,1);
            $('#givenTicketsZone').find('label').eq(itemIndex).fadeOut().remove();
        }
    }
    
    $(wholeTicketObject).fadeOut().remove();
    if(chosenNumbers.length<1){
        $('#givenTicketsZone').fadeOut();
        $('#currentTickets')
            .append('<p class="text-muted">Aqui apareceran los tickets que pertenezcan a tu compra</p>'); 
        $('#folioForm').find('fieldset').prop('disabled',true);
    }   
    console.log(givenNumbers,chosenNumbers);
}

function loadTickets(){
    for (let index = 0; index <= ticketsToRender; index++) {
        let number=addCeros((index),ticketAmount.toString().length);
        if(ocupiedNumbers.toString().includes(number)){
            continue;
        }
        $('#ticketsContainer').append(
        '<label type="button" class="border border-2 rounded-pill p-3 m-1 "'
        +' onClick="addNumber(this)" id="'+number+'" >'
        +number+'</label>');
    }
}
function addNumber(who){
    if(givenNumbers.length){
        
    }
    if(chosenNumbers.length<ticketLimit){
    $('#folioForm').children('fieldset').removeAttr('disabled');
    $(who).addClass('bg-secondary text-light disabled');
    var ticketNumber=addCeros(who.id,ticketAmount.toString().length);
    var ticketToAdd= 
        '<label class="position-relative" id="'+ticketNumber+'">'
            +'<img src="https://img.icons8.com/color/96/000000/ticket.png"/>'
            +'<span class="ticketNumberIcon">'+ticketNumber+'</span>'
            +'<a type="button" class="cancelNumber bg-transparent border-none" onclick="removeNumber(this)">'
            +'<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" '
                +'width="24" height="24" viewBox="0 0 48 48" style=" fill:#000000;">'
                +'<path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path>'
                +'<path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path>'
                +'<path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>'
                +'</svg>'
                +'</a>'
            +'</label>'; 
            $('#currentTickets').find('p').fadeOut();
            $(ticketToAdd).appendTo('#currentTickets').hide().fadeIn('slow');
            if(chances>1){
                $('#givenTicketsZone').show();
                var numbersGenerated=freeNumbers();
                givenNumbers= givenNumbers.concat(numbersGenerated);
                numbersGenerated.forEach(number => {
                    var ticketNumber=addCeros(number,ticketAmount.toString().length);
                    var ticketToAdd= 
                    '<label class="position-relative" id="'+ticketNumber+'">'
                        +'<img src="https://img.icons8.com/color/96/000000/ticket.png"/>'
                        +'<span class="ticketNumberIcon">'+ticketNumber+'</span>'
                        +'</label>';
                        $(ticketToAdd).appendTo('#givenTicketsZone').hide().fadeIn('slow');
                        });
            }
            chosenNumbers.push(ticketNumber);
    }else{
        alert('Se ha alcanzado el limite de Numeros por comprar...')
    }
    
}
function freeNumbers() {
    var numbersArray=[];
    if(!ocupiedNumbers.length&&!givenNumbers.length){
        while(numbersArray.length<chances-1){
            var min= (ticketsToRender+(ticketsToRender*numbersArray.length))+1;
            var max=min+ticketsToRender;
            const number = Math.floor(Math.random() * (max - min )) + min;
            numbersArray.push(number);
        }   
        return numbersArray;
    }else{
        while(numbersArray.length<chances-1){
            var min= (ticketsToRender+(ticketsToRender*numbersArray.length))+1;
            var max=min+ticketsToRender;
            const number = Math.floor(Math.random() * (max - min )) + min;
            if(!ocupiedNumbers.includes(number)&&!givenNumbers.includes(number)){
                numbersArray.push(number);
            }
        }
        return numbersArray;
    }
}

function addCeros(number,requiredDigits) {
    var currentNumber=number.toString();
    return currentNumber.length < requiredDigits ? addCeros("0" + currentNumber, requiredDigits) : currentNumber;
}

function validationConfig() {
    jQuery.validator.addMethod( "phone", function( phone_number, element ) {
        phone_number = phone_number.replace( /\s+/g, "" );
        return this.optional( element ) || phone_number.length > 9 &&
            phone_number.match( /^\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
    }, "El necesita tiene un formato de 123-123-1212" );
    jQuery.validator.addMethod( "notEqualTo", function( value, element, param ) {
        return this.optional( element ) || value !=param;
    }, "El valor por defecto no esta permitido en el Estado de Residencia" );

    $('#folioForm').validate({
        rules:{
            fnames:{
                required:true,
            },
            lnames:{
                required:true,
            },
            cellphone:{
                required:true,
                phone:true,
            },
            state:{
                required:true,
                notEqualTo:'default'
            }
        },
        errorElement:"h5",
        errorLabelContainer: "#errContainer",
        wrapper: "li",
        focusCleanup: true,
        highlight: function(element, errorClass, validClass) {
            $(element).fadeOut(()=> {$(element).fadeIn();});
            $(element).addClass(errorClass).removeClass(validClass);
            },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            },        
        messages:{
            fnames:{
                required:"Por favor, ingresa tu(s) nombre(s)."
            },
            lnames:{
                required:"Se espera tu(s) apellido(s) "
            },
            cellphone:{
                required:"Su celular es necesario para contactarlo",
                phoneUs:"El formato de celular deberia ser: 123-123-1212",
            }
        }
    });
}